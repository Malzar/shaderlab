﻿Shader "Custom/Shield" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Displace("Shield displacement", range(0,1))=1
	}
	SubShader {
		Tags { "RenderType"="Opaque"}
		LOD 200	
		
		CGPROGRAM		
		#pragma surface surf Standard fullforwardshadows
		#pragma 3.0
		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _MainTex;
		half _Glossiness;
		half _Metallic;

		UNITY_INSTANCING_BUFFER_START(Props)
		
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG 
		
		Tags { "Queue"="Transparent" }
		ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		
		#pragma surface surf Standard fullforwardshadows vertex:vert
		sampler2D _MainTex;
		fixed4 _Color;
		half _Displace;

		struct Input {
			float2 uv_MainTex;
		};

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
		};

		void vert(inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);

			v.vertex = v.vertex + float4(v.normal,1)*_Displace;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = _Color;
			o.Alpha = 0;
		}
		ENDCG
		
			
	}
	FallBack "Diffuse"
}
