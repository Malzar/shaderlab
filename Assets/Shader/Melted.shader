﻿Shader "Malzar/Melted" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		//_High ("Melted point", Float) = 0
		_High("Melted point",Range(0,1.5))=0
		_ColorMelted("Melted Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		ZWrite ON

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _ColorMelted;
		half _High;

		struct Input {
			float2 uv_MainTex;
			half percentaje;
			half vMod;
		};

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
		};

		void vert(inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.percentaje = (v.vertex.y-_High)/v.vertex.y;
			v.vertex.y =(v.vertex.y>_High)*(v.vertex.y* o.percentaje)
						+ (v.vertex.y<_High)*v.vertex.y*0.001;
			o.vMod = (v.vertex.y<_High);
		}
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * (1-IN.vMod) + (_ColorMelted+c.rgb*IN.percentaje)*IN.vMod;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness * (1-IN.vMod) + (1-IN.percentaje)*IN.vMod;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
