﻿Shader "Malzar/Miyagi"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "green" {}
        _Colorout("Color",Color)= (1,1,1,1)
        _ScaleX("Scale map xAxe ",float) =1
        _ScaleY("Scale map yAxe ",float) =1
        _OffsetX("Offset map xAxe ",float) =0
        _OffsetY("Offset map yAxe ",float) =0

        _Propierty1("Saturation",float) =0
        _Propierty2("bigthness",float) =0
        _Propierty3("Propierty 3",float) =0
        _Propierty4("Propierty 4",float) =0
        _Propierty5("Propierty 5",float) =0
        _Propierty6("Propierty 6",float) =0

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows vertex:vert

        #pragma target 3.0

        sampler2D _MainTex;
        fixed4 _Colorout;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 localPos;
        };

        half _ScaleX;
        half _ScaleY;
        half _OffsetX;
        half _OffsetY;
        half _Propierty1;
        half _Propierty2;
        half _Propierty3;
        half _Propierty4;
        half _Propierty5;
        half _Propierty6;

        static const float _PI = 3.141592;

        void vert(inout appdata_full v, out Input o){
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.localPos = v.vertex.xyz;
        }

        float drawPoint(float2 pos,float pct){
            return  smoothstep( pct-0.02, pct, pos.y) -
                    smoothstep( pct, pct+0.02, pos.y);
        }

        float drawLine(float pos,float ini, float size){
            half halfSize = size*0.5;
            return smoothstep(ini-halfSize,ini-halfSize*0.9,pos)-
                    smoothstep(ini+halfSize*0.9,ini+halfSize,pos);
        }

        float drawLine(float2 pos,float2 ini, float dir,float size){
            half halfSize = size*0.5;
            half dirNormalized = dir>0;
            return ((1-dirNormalized)+(dir*step(ini.x,pos.x)))*
             (step(ini.y-halfSize,pos.y)-step(ini.y+halfSize,pos.y));
        }

        float drawRectangle(float2 pos,float2 ini, float2 size){
            return (step(ini.x,pos.x)-step(ini.x+ size.x,pos.x))*
            (step(ini.y,pos.y)-step(ini.y+size.y,pos.y));
        }

        float drawCircle(float2 pos, float2 center, float radius){
            float dist = distance(pos.xy,center);
            //return step(dist,radius);
            return smoothstep(dist*0.99,dist*1.01,radius);
        }

        float heartRithm(){
            return pow((0.5+0.5*sin(2*3.141592*_Time.y)),4);
        }

        float3 hsb2rgb(in float3 c){
            float3 rgb = clamp(abs(fmod(c.x*6.0+float3(0.0,4.0,2.0),
                            6.0)-3.0)-1.0,0.0,1.0 );
            rgb = rgb*rgb*(3.0-2.0*rgb);
            return c.z * lerp(float3(1.0,1.0,1.0), rgb, c.y);
        }

        float3 circlePolarColor(float2 pos, float2 center, float radius){
            //Polar coordinates
            float2 direction =pos-center;
            float angle = atan2(direction.y,direction.x);
            float rcolor = length(direction)/radius;

            return hsb2rgb(float3(angle/(2.0*_PI)+0.5,rcolor,1.0));
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float3 lPos = IN.localPos;
            float3 wPos = IN.worldPos;
            //Canvas position NOTE:remember, the uv are inverted
            //NOTE 2: the center is the 0,0 point
            float3 cPos =float3(((1-IN.uv_MainTex.x)-0.5)*_ScaleX+_OffsetX,0,((1-IN.uv_MainTex.y)-0.5)*_ScaleY+_OffsetY);
            //Normalized canvas position
            float3 nCPos = float3((1-IN.uv_MainTex.x),0,(1-IN.uv_MainTex.y));        



            float2 center = float2(0,0);
            float p = pow(cPos.x,0.5);

            float d = abs(distance(cPos.xz,center));
            float r =_Propierty1;//+ abs(1*heartRithm());
            float circle= drawCircle(cPos.xz,center,r);

            

            float3 color =float3(0,0,0);
            color = circle*circlePolarColor(cPos.xz,center,r)+(1-circle)*color;
            circle =drawPoint(cPos.xz,p);
            color = circle * float3(1,1,1)+(1-circle)*color;
            o.Albedo = color;

            float luminance;
            float3 fragcolor;
            float3 c;
            if(IN.uv_MainTex.x>(1.0/3.0)||(IN.uv_MainTex.y<(1.0/3.0))){
                fragcolor = tex2D (_MainTex, IN.uv_MainTex);
                luminance = (fragcolor.r+fragcolor.g+fragcolor.b)/3.0;
                c.r=luminance;
                c.g=luminance;
                c.b=luminance;
                c*=_Colorout;
            }else{
                //c =tex2D (_MainTex, IN.uv_MainTex)*_Colorout;
                c=color;
            }
            o.Albedo = c;

            o.Metallic = 0;
            o.Smoothness = 0;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
