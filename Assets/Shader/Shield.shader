﻿Shader "Malzar/Shield"{
    Properties{
        [Header(Shield)]
        _ShieldTex("Shield pattern", 2D) = "white" {}
        _ShieldColor("Shield color", Color) = (1,1,1,1)
        _ShieldDistance("Shield distance", Range(0,0.1)) = 0.05
        _ShieldIntensity("Shield intensity",Range(0,4))=2
        [Header(Impact effect)]
        _ImpactSize("Impact effect max distance",float) =1
        _ImpactIntensity("Impact intensity effect",float) =1
        [Toggle(WAVES)]
        _isWave("Enable wave impact effect", float)=0
    }
    SubShader{      
        Tags { "Queue"="Transparent" "RenderType"="Transparent"  }
        LOD 200
        //ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf Lambert fullforwardshadows vertex:vert alpha:fade
        #pragma target 3.0
        #pragma shader_feature WAVES       
        #include "UnityCG.cginc"

        sampler2D _ShieldTex;
        fixed4 _ShieldColor;
        half _ShieldDistance;
        half _ShieldIntensity;
        fixed _ImpactSize;
        fixed _ImpactIntensity;

        int _PointSize;
        float4 _Points[50];

        struct appdata{
            float4 vertex : POSITION;
            float2 texcoord : TEXCOORD0;
            float3 normal : NORMAL;
        };

        struct Input{
            float2 uv_ShieldTex;      
            float3 worldNormal; 
            float3 viewDir;
            float3 localShieldPos;
        };
 
        float GetEmissionByCollision(float3 objPos){
            float emission =0;
            float distan; 
 
            for(int i=0;i<50;i++){
                if(i==_PointSize){
                    return emission; 
                }
                emission +=frac(1- max(0,(_Points[i].w*_ImpactSize-distance(_Points[i].xyz,objPos)))/_ImpactSize)*(1-_Points[i].w);
            }
            return emission; 
        }

        void vert (inout appdata v, out Input o){ 
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.localShieldPos = (v.vertex+float4(v.normal,1)*_ShieldDistance).xyz;
            #if WAVES
                half wave= GetEmissionByCollision(o.localShieldPos);
                _ShieldDistance*= clamp(1- (wave*wave),0,1);
            #endif
            v.vertex = v.vertex+float4(v.normal,1)*_ShieldDistance;
            o.worldNormal = v.normal;
            
        }

        void surf (Input IN, inout SurfaceOutput  o){
            fixed4 shieldT= tex2D(_ShieldTex,IN.uv_ShieldTex);            
            float fressnel = dot(IN.worldNormal,IN.viewDir);
            fressnel = saturate(1-fressnel);
            fressnel = pow(fressnel,4-_ShieldIntensity);
            o.Albedo = fixed3(0.5,0.5,0.5);

            //////////////////Avoid fressnel effect
            //fressnel =0;
            ////////////////////////////////////////

            float emission=GetEmissionByCollision(IN.localShieldPos);            
            emission*=_ImpactIntensity;
            emission = max(fressnel,emission);
            o.Emission=_ShieldColor *  shieldT.r* emission;
            o.Alpha =_ShieldColor.a* shieldT.r *emission ;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
