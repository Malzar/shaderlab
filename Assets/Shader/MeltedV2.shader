﻿Shader "Malzar/MeltedV2" {
	Properties {
		//_Height ("Melted point", Float) = 0
		[Header(Melted)]
		_MinHeight("Minimun height",float) = 0
		_Height("Melted height",Range(0.1,2))=0
		_MeltDistance("Meltd distance", float) = 2
		_ColorMelted("Melted Color", Color) = (1,1,1,1)
		[Header(Standar)]
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		ZWrite ON

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _ColorMelted;
		half _Height;
		half _MinHeight;
		half _MeltDistance;

		struct Input {
			float2 uv_MainTex;
			half percentaje;
			half vMod;
		};

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
		};


		float4 getMeltedPosition(float4 objectLocalPosition, float3 objectLocalNormal){
			float4 worldPosition = mul(unity_ObjectToWorld,objectLocalPosition);
			float4 worldNormal = mul(unity_ObjectToWorld,objectLocalNormal);

			float meltedPointPasssed=worldPosition.y<_MinHeight;
			worldPosition.y=worldPosition.y*(1-meltedPointPasssed)+ _MinHeight*meltedPointPasssed;

			float meltPercentaje = smoothstep(_MinHeight,_MinHeight+_Height,worldPosition.y);
			meltPercentaje = (1-saturate(meltPercentaje))*_MeltDistance;
			worldPosition.xz += normalize(worldNormal.xz)*meltPercentaje;

			return(mul(unity_WorldToObject,worldPosition));
		}

		void vert(inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o); 

			v.vertex = getMeltedPosition(v.vertex,v.normal);

		}
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * (1-IN.vMod) + (_ColorMelted+c.rgb*IN.percentaje)*IN.vMod;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness * (1-IN.vMod) + (1-IN.percentaje)*IN.vMod;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
