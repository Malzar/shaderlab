﻿Shader "Malzar/BurnEffect"
{
    Properties
    {
        [Header(Glow)]
        [HDR]_GlowColor ("Glow Color", Color) = (1,1,1,1)
        _GlowRange("Range", Range(0, 0.5)) = 0.1
        _GlowFalloff("Falloff", Range(0.0001, 1)) = 0.1
        [Header(Incandescent)]
        [HDR]_IncandescentColor ("Incandescent color", Color) = (1,1,1,1)
        _Amplitude("Speed", Range (0,1))= 0.5
        //_DisplacementX("Dissplacement x", Range (0,1))= 1        
        //_DisplacementY("Disslacement y", Range (0,1))= 1
        [Header(Dissolved)]
        _DissolvedTex("Dissolved texture (B/W)", 2D)="wite" {}
        _DissolveAmount("Dissolve amount", Range(0,1)) = 1
        [Header(Standar)]
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        Cull Off
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0


        struct Input
        {
            float2 uv_MainTex;
            float2 uv_DissolvedTex;
            float3 worldPos;
        };
        sampler2D _MainTex;
        sampler2D _DissolvedTex;

        fixed3 _GlowColor;
        fixed3 _IncandescentColor;
        half _DissolveAmount;
        float _GlowRange;
        float _GlowFalloff; 
        float _Amplitude;
       // half _DisplacementX;
        //half _DisplacementY;
        
        fixed4 _Color;
        half _Glossiness;
        half _Metallic;


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            //Dissolver
            fixed4 dissolve = tex2D(_DissolvedTex,IN.uv_DissolvedTex);
            dissolve *=0.9999;
            float invisible = dissolve.x - _DissolveAmount;            
            clip(invisible);
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
            //incandescent      
            float glow = smoothstep(_GlowRange+_GlowFalloff,_GlowRange,invisible);
            // float IglowV = (cos((IN.worldPos.x*_Amplitude)+_SinTime.w*_DisplacementX)+1)*0.5;
            // float IglowH = (sin((IN.worldPos.x*_Amplitude)+_CosTime.w*_DisplacementY)+1)*0.5;            
            //float Incandescent = max((IglowV-IglowH)*glow,0);

            float IglowV = _Time.y*_Amplitude;
            float IglowH = _SinTime.w*_Amplitude; 
            float Ivalue = tex2D(_DissolvedTex,float2(IN.uv_DissolvedTex.x+IglowH,IN.uv_DissolvedTex.y+IglowV)).x;
            float Incandescent = max((Ivalue)*glow,0);

            o.Emission = Incandescent*_IncandescentColor+glow*_GlowColor;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
