﻿Shader "Malzar/FearEfect" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_SecondColor("Taained color", Color)= (1,0,0,1)
		_Intensity("Tained intensity", Range (0,4))= 1
		_Gradient ("Tained gradient", Range(0,2)) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows 
		#pragma vertex vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float tainted;
		};

		half _Gradient;
		fixed4 _Color;
		fixed4 _SecondColor;
		half _Intensity;

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
		};

		void vert (inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);
			fixed3 Npos = mul (unity_ObjectToWorld, v.normal);
			fixed tain = (dot(normalize(Npos),float3(0,-1,0))+1)*0.5;
			tain =(tain*tain)* _Gradient;
			o.tainted = tain;
		}

		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb+(IN.tainted*_SecondColor*_Intensity);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
