﻿Shader "Malzar/PietM-Tableau"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        float drawLine(float pos,float ini, float size){
            half halfSize = size*0.5;
            return smoothstep(ini-halfSize,ini-halfSize*0.9,pos)-
                    smoothstep(ini+halfSize*0.9,ini+halfSize,pos);
        }

        float drawLine(float2 pos,float2 ini, float dir,float size){
            half halfSize = size*0.5;
            half dirNormalized = dir>0;
            return ((1-dirNormalized)+(dir*step(ini.x,pos.x)))*
             (step(ini.y-halfSize,pos.y)-step(ini.y+halfSize,pos.y));
        }

        float drawRectangle(float2 pos,float2 ini, float2 size){
            return (step(ini.x,pos.x)-step(ini.x+ size.x,pos.x))*
            (step(ini.y,pos.y)-step(ini.y+size.y,pos.y));
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float2 st = (IN.uv_MainTex);
            float size =8.0;
            //remember, the uv are inverted
            float3 pos =float3(((1-st.x)*size)-(size*0.5),0,((1-st.y)*size)-(size*0.5));

            float rectangleBackground = drawRectangle(pos.xz,float2(-4,-4),float2(8,8));
            float rectangleRed = drawRectangle(pos.xz,float2(-4,1.5),float2(1.25,3));
            float rectangleYellow = drawRectangle(pos.xz,float2(3.5,1.5),float2(0.5,3));
            float rectangleBlue = drawRectangle(pos.xz,float2(2.25,-4),float2(1.75,1));
            float line1 = drawLine(pos.z,1.5,0.25);
            float line2 = drawLine(pos.z,2.75,0.25);
            float line3 = drawLine(pos.x,-2.65,0.25);
            float line4 = drawLine(pos.x,3.5,0.25);
            float line5 = drawLine(pos.x,2.25,0.25);
            float line6 = drawLine(pos.xz,float2(-2.65,-3),1,0.25);
            float line7 = drawLine(pos.zx,float2(1.5,-3.5),1,0.25);

            //float line2= drawLine2(pos.xz,float2(0.5,1),1,0.25);  

            float3 color = rectangleBackground * float3(0.8,0.8,0.5)+(1-rectangleBackground)*float3(0,0,0);
            color = rectangleRed*float3(1,0,0)+(1-rectangleRed)*color;
            color = rectangleYellow*float3(1,0.8,0.1)+(1-rectangleYellow)*color;            
            color = rectangleBlue*float3(0,0,1)+(1-rectangleBlue)*color;

            color = line1*float3(0,0,0)+(1-line1)*color;
            color = line2*float3(0,0,0)+(1-line2)*color;
            color = line3*float3(0,0,0)+(1-line3)*color;
            color = line4*float3(0,0,0)+(1-line4)*color;
            color = line5*float3(0,0,0)+(1-line5)*color;
            color = line6*float3(0,0,0)+(1-line6)*color;
            color = line7*float3(0,0,0)+(1-line7)*color;

            o.Albedo = color;


            o.Metallic = 0;
            o.Smoothness = 0;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
