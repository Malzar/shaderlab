﻿Shader "Malzar/DissolveEffect"
{
    Properties
    {
        [Header(Glow)]
        [HDR]_GlowColor("Color", Color) = (1, 1, 1, 1)
        _GlowRange("Range", Range(0, 0.5)) = 0.1
        _GlowFalloff("Falloff", Range(0.0001, 1)) = 0.1
        [Header(Dissolved)]
        _DissolvedTex("Dissolved texture (B/W)", 2D)="wite" {}
        _DissolveAmount("Dissolve amount", Range(0,1)) = 1
        [Header(Standar)]
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _DissolvedTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_DissolvedTex;
        };

        half _DissolveAmount;
        float3 _GlowColor;
        float _GlowRange;
        float _GlowFalloff;        
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            fixed4 dissolve = tex2D (_DissolvedTex, IN.uv_DissolvedTex) * _Color;
            dissolve *= 0.999;
            float invisible = _DissolveAmount-dissolve;
            float glowVissible = (invisible+1)*0.5;
            float3 glow = smoothstep(_GlowRange+_GlowFalloff,_GlowRange,invisible)*_GlowColor;
            clip(invisible);
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;            
            o.Emission = glow;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
