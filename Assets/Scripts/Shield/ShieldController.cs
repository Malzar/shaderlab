﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShieldController : MonoBehaviour {
    private static readonly Vector4 [] DefaultCollisionPoints = new Vector4 [] { new Vector4(0, 0, 0, 0) };

    [SerializeField]
    private Material _shieldMaterial;
    private List<Vector4> _points;
    private float _collisionLife = 1;

    private void Awake() {
        _points = new List<Vector4>();
    }

    private void Start() {
        //initialize the shader vector with the higher value (remember, not dinamyc sized array are allowed)
        _shieldMaterial.SetVectorArray("_Points", new Vector4 [50]);
    }

    void Update() {
        UpdateCollisionsPoints();
        SetCollisionPoints();
    }

    private void UpdateCollisionsPoints() {
        _points = _points
            .Select(point => new Vector4(point.x, point.y, point.z, point.w + (Time.deltaTime / _collisionLife)))
            .Where(point => point.w <= 1).ToList();
    }

    public void AddCollision(Vector3 postition) {
        _points.Add(postition);
    }

    private void SetCollisionPoints() {
        if(_shieldMaterial) {
            _shieldMaterial.SetFloat("_PointSize", _points.Count);
            if(_points.Count < 1) {
                _shieldMaterial.SetVectorArray("_Points", DefaultCollisionPoints);
            } else {
                _shieldMaterial.SetVectorArray("_Points", _points.ToArray());
            }
        }
    }
}
