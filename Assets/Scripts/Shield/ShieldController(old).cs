﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShieldControllerOLD : MonoBehaviour {
    private static readonly Vector4 [] DefaultVectorCollision = new Vector4 [] { new Vector4(0, 0, 0, 0) };
    public Vector4[] Points;
    public Material ShieldMaterial;
    public bool Enable { get { return p_ShieldEnable; } set { EnableShield(value); } }
    [SerializeField]
    private bool p_ShieldEnable;
    private Renderer MRender;

    void Awake() {
        if(gameObject.GetComponent<SkinnedMeshRenderer>() != null)
            MRender = gameObject.GetComponent<SkinnedMeshRenderer>();
        else if(gameObject.GetComponent<MeshRenderer>() != null)
            MRender = gameObject.GetComponent<MeshRenderer>();
        else
            throw new System.InvalidOperationException("The shield needs a renderer component");
#if UNITY_EDITOR
        ShieldMaterial = MRender.sharedMaterials [1];
#else
        ShieldMaterial = MRender.materials [1];
#endif
        EnableShield(true);
    }

    void Update() {
        SetPointsColision();
    }

    public void AddColision(Vector3 position) {

    }

    private void EnableShield(bool enable) {
        if(!enable && p_ShieldEnable) {
            List<Material> mats = new List<Material>();
            MRender.GetMaterials(mats);
            mats.Remove(ShieldMaterial);
            MRender.materials = mats.ToArray();
            p_ShieldEnable = false;
        } else if(enable && !p_ShieldEnable) {
            List<Material> mats = new List<Material>();
            MRender.GetMaterials(mats);
            mats.Add(ShieldMaterial);
            MRender.materials = mats.ToArray();
            p_ShieldEnable = true;
        }
    }


    private void SetPointsColision() {
        if(Points.Length < 1) {
            ShieldMaterial.SetInt("_PointSize", DefaultVectorCollision.Length);
            ShieldMaterial.SetVectorArray("_Points", DefaultVectorCollision);
        } else {
            ShieldMaterial.SetInt("_PointSize", Points.Length);
            ShieldMaterial.SetVectorArray("_Points", Points);
        }
    }
}
