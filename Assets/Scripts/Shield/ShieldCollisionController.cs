﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShieldController))]
public class ShieldCollisionController : MonoBehaviour
{
    [SerializeField]
    ShieldController _shieldC;

    private void OnCollisionEnter(Collision collision) {
        for(int i = 0; i < collision.contactCount; i++) {
            _shieldC.AddCollision(
                TransformToLocalPosition(
                    collision.GetContact(i).point));
        }
        //Destroy(collision.gameObject);
    }

    private Vector3 TransformToLocalPosition(Vector3 point) {
        Matrix4x4 transformMatrix = transform.localToWorldMatrix;
        transformMatrix = Matrix4x4.Inverse(transformMatrix);

        return transformMatrix * point;
    }
}
