﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSimulation : MonoBehaviour
{
    [SerializeField]
    private ShieldController _shieldC;
    [SerializeField]
    private float _shootCooldown;
    [SerializeField]
    private float _radius;
    private float _timer;

    void Update()
    {
        _timer += Time.deltaTime;
        while(_timer > _shootCooldown && _shootCooldown>0) {
            _timer -= _shootCooldown;
            _shieldC.AddCollision(GeneratePoint());
        }
        
    }

    private Vector3 GeneratePoint() {
        Vector3 point;
        point = Random.onUnitSphere * _radius;
        Debug.Log(point);
        return point;
    }
}
