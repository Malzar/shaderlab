﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChrismasController : MonoBehaviour
{
    public float timeToBurn;
    public AnimationCurve lightIntensityCurve;
    public AnimationCurve lightColorCurve;
    public AnimationCurve BurnCurve;
    public Color lightColor;
    public Light chrismasLight;
    public Material chrismasTreeMaterial;
    public List<Rigidbody> rigidbodies;

    private float m_timeToFall;
    private float m_timer;
    private bool m_falling;
    private float m_timePercentaje;
    // Start is called before the first frame update
    void Awake()
    {
        foreach (Rigidbody elem in rigidbodies) {
            EnablePhysic(elem, false);
        }
    }

    private void Start() {
        m_timer = 0;
        m_falling = false;
        m_timeToFall = timeToBurn * 0.45f;
    }

    private void EnablePhysic(Rigidbody rigidBody, bool useGravity) {
        if (useGravity)
            rigidBody.WakeUp();
        else
            rigidBody.Sleep();
    }

    // Update is called once per frame
    void Update()
    {
        TimeringControll();
        LigthControll();
        PhysicsControll();
        ShaderControll();
    }

    private void TimeringControll() {
        m_timer += Time.deltaTime;
        float percentaje = m_timer / timeToBurn;
        m_timePercentaje = Mathf.Clamp(percentaje, 0, 1);
    }

    private void LigthControll() {
        chrismasLight.intensity = lightIntensityCurve.Evaluate(m_timePercentaje);
        chrismasLight.color =Color.Lerp(Color.white ,lightColor, lightColorCurve.Evaluate(m_timePercentaje));
    }

    private void PhysicsControll() {
        if (m_timer > m_timeToFall && !m_falling) {
            foreach (Rigidbody elem in rigidbodies) {
                EnablePhysic(elem, true);
            }
            m_falling = true;
        }
    }

    private void ShaderControll() {
        chrismasTreeMaterial.SetFloat("_DissolveAmount", m_timePercentaje);
    }
}
